# OpenML dataset: StackOverflow-polarity-test

https://www.openml.org/d/43148

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A Gold Standard of ~4,400 questions, answers, and comments from Stack Overflow, manually annotated for polarity. The dataset has been used for developing the EMTk toolkit for polarity detection from technical text.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43148) of an [OpenML dataset](https://www.openml.org/d/43148). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43148/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43148/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43148/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

